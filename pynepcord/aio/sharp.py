# -*- coding: utf-8 -*-

# MIT License
#
# Copyright (c) 2020 NellyD3v
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import aiohttp
from datetime import datetime
from asyncio.events import AbstractEventLoop

from pynepcord import SHARP_API
from pynepcord import __version__
from pynepcord.models import SharpEntry
from pynepcord.errors import InvalidUserID
from pynepcord.errors import UserNotFound
from pynepcord.errors import Unauthorized
from pynepcord.errors import Forbidden
from pynepcord.errors import Unprocessable


class SharpSession:
    """Create a new async NeppedCord Sharp session.

    Is in development!!!

    Methods
    -------
    :coroutine: check(user_id: int)
        Check if given user is in Sharp Database.
    :coroutine: ban(user_id: int, reason: str, image: Optional[str])
        Bans user/guild/bot. You must be a trusted developer to use it.
    :coroutine: unban(user_id: int)
        Unbans user/guild/bot. You must be a developer of the API to use it.
    """
    def __init__(self, api_key, asyncio_loop: AbstractEventLoop=None):
        """Init a new sharp session for NeppedCord API.

        Parameters
        ----------
        api_key : str
            NeppedAPI key to access their service.
        asyncio_loop : Optional[asyncio.events.AbstractEventLoop]
            Asyncio event loop.
        """
        self.api_key = api_key
        self.session = aiohttp.ClientSession(loop=asyncio_loop)

    def __check_user_id(self, user_id: int) -> bool:
        """Check if given user ID is correct.

        Parameters
        ----------
        user_id : int
            User ID that you need to check.

        Returns
        -------
        True
            If the user ID is correct.
        False
            If the user ID is incorrect.
        """
        return (
            (isinstance(user_id, str) and user_id.isdigit()) or 
            (isinstance(user_id, int) and user_id > 0)
        )

    async def check(self, user_id: int) -> SharpEntry:
        """Check if given user/guild/bot is in Sharp Database.

        :coroutine:

        Parameters
        ----------
        user_id : int
            Target discord user/guild/bot id.

        Returns
        -------
        pynepcord.models.SharpEntry
            Object that contains data about given user/guild/bot from Sharp.

        Raises
        ------
        pynedcord.errors.InvalidUserID
            Raises when given user/guild/bot ID is incorrect.
        pynepcord.errors.UserNotFound
            Raises when given user/guild/bot wasn't found in Sharp Database.
        pynepcord.errors.Unauthorized
            Raises when API key is incorrect.
        """
        if not self.__check_user_id(user_id):
            raise InvalidUserID("User/guild/bot ID must be correct.")

        async with self.session.get(
            SHARP_API + str(user_id),
            headers={
                'Authorization': self.api_key,
                'User-Agent': "pynepcord/" + __version__
            }
        ) as response:
            resp_json = await response.json()

        if resp_json.get('error') and response.status == 401:
            raise Unauthorized("Your API key is incorrect. Get one at https://neppedcord.top/panel")

        if resp_json.get('error') and response.status == 404:
            raise UserNotFound(f"The user/guild/bot with ID '{user_id}' isn't found!")

        resp_userID = int(resp_json['userID'])
        resp_modID = int(resp_json['moderatorID'])
        resp_banReason = resp_json['banData']['reason']
        resp_imageURL = resp_json['banData']['image']
        resp_dateTime = None
        if resp_json['banData']['dateTime'] is not None:
            resp_dateTime = datetime.fromtimestamp(resp_json['banData']['dateTime'])

        return SharpEntry(
            user_id=resp_userID,
            moderator_id=resp_modID,
            reason=resp_banReason,
            image=resp_imageURL,
            code=response.status,
            time=resp_dateTime
        )


    async def ban(self, user_id: int, reason: str=None, image: str=None) -> bool:
        """Add user/guild/bot to the Sharp Database.

        :coroutine:

        You must be a trusted developer to use this method!!!

        Parameters
        ----------
        user_id : int
            Target discord user/guild/bot id.
        reason : str
            Ban reason.
        image : str
            URL or Base64-encoded image data.

        Returns
        -------
        True
            If user/guild/bot added to Sharp successfully.
        False
            If everything is OK, but service respond with False.

        Raises
        ------
        pynedcord.errors.InvalidUserID
            Raises when given user/guild/bot ID is incorrect.
        pynepcord.errors.Unauthorized
            Raises when API key is incorrect.
        pynepcord.errors.Forbidden
            Raises when you have no permissions to add users/guilds/bots.
        pynepcord.errors.Unprocessable
            Raises when current user/guild/bot already exist in Sharp.
        """
        if not self.__check_user_id(user_id):
            raise InvalidUserID("User ID must be correct.")

        async with self.session.post(
            SHARP_API + str(user_id) + '/ban',
            headers={
                'Authorization': self.api_key,
                'User-Agent': "pynepcord/" + __version__
            },
            json={
                'reason': reason,
                'image': image
            }
        ) as response:
            resp_json = await response.json()

        if resp_json.get('error') and response.status == 401:
            raise Unauthorized("Your API key is incorrect. Get one at https://neppedcord.top/panel")

        if resp_json.get('error') and response.status == 403:
            raise Forbidden(f"You don't have permissions to use this method!")

        if resp_json.get('error') and response.status == 422:
            raise Unprocessable(f"The user/guild/bot with ID '{user_id}' already banned!")

        return resp_json['banned']


    async def unban(self, user_id: int) -> bool:
        """Remove user/guild/bot from the Sharp Database.

        :coroutine:

        You must be a developer of the API to use this method!!!

        Parameters
        ----------
        user_id : int
            Target discord user/guild/bot id.

        Returns
        -------
        True
            If user/guild/bot removed from Sharp successfully.
        False
            If everything is OK, but service respond with False.

        Raises
        ------
        pynedcord.errors.InvalidUserID
            Raises when given user/guild/bot ID is incorrect
        pynepcord.errors.Unauthorized
            Raises when API key is incorrect.
        pynepcord.errors.Forbidden
            Raises when you have no permissions to add users/guilds/bots.
        pynepcord.errors.UserNotFound
            Raises when given user/guild/bot wasn't found in Sharp Database.
        """
        if not self.__check_user_id(user_id):
            raise InvalidUserID("User ID must be correct.")

        async with self.session.delete(
            SHARP_API + str(user_id) + '/unban',
            headers={
                'Authorization': self.api_key,
                'User-Agent': "pynepcord/" + __version__
            }
        ) as response:
            resp_json = await response.json()

        if resp_json.get('error') and response.status == 401:
            raise Unauthorized("Your API key is incorrect. Get one at https://neppedcord.top/panel")

        if resp_json.get('error') and response.status == 403:
            raise Forbidden(f"You don't have permissions to use this method!")

        if resp_json.get('error') and response.status == 404:
            raise Unprocessable(f"The user/guild/bot with ID '{user_id}' wasn't banned!")

        return resp_json['unbanned']
