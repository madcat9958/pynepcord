# PyNepCord Images Examples

### Random image by category
Available categories:
`baka`, `cry`, `cuddle`, `dance`, `happy`, `hug`, `kiss`, `pat`, `poke`, `sad`, `smug`, `wag`
 
```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

from pynepcord.base import ImageSession


if __name__ == '__main__':
    key = 'your-api-token'
    session = ImageSession(key) # Create a session to get images
    image = session.get_image('happy') # get the image by the 'happy' category
    print(image.url) # display the URL of the picture in the terminal
```

### Random image by ... random category?

```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

from pynepcord.base import ImageSession


if __name__ == '__main__':
    key = 'your-api-token'
    session = ImageSession(key)
    image = session.get_image('random') # if you specify a 'random' category, it will be selected at random
    print(image.url) # display the URL of the picture in the terminal
    print(image.category) # display the category name
```

### async/await variant
```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

import asyncio
from pynepcord.aio import ImageSession
# pynepcord.base - the non-async version that uses `requests`.
# pynepcord.aio - same, but uses `aiohttp` and async/await syntax.


async def main():
    key = 'your-api-token'
    session = ImageSession(key)
    image = await session.get_image('happy')
    print(image.url)

if __name__ == '__main__':
    asyncio.run(main())
```

# PyNepCord Sharp Examples

### Checks if the user in the sharp
```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

from pynepcord.base import SharpSession
from pynepcord.errors import UserNotFound

if __name__ == '__main__':
    key = 'your-api-token'
    session = SharpSession(key)
    try:
        user = session.check(560529834325966858) # Checks if the user with ID 560529834325966858 in Sharp
    except UserNotFound: # If the user wasn't found, it returns an error
        print("The user wasn't found :klass:")
    else:
        print(user)
```

### Bans user
```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

# Note that you MUST be a trusted developer to use this method!

from pynepcord.base import SharpSession

if __name__ == '__main__':
    key = 'your-api-token'
    session = SharpSession(key)
    user = session.ban(
        560529834325966858, 
        reason="Toxic",
        image=None # You can insert Base64 encoded image or a link to the image.
    ) # Bans the user with ID 560529834325966858 for reason "Toxic" without an image.
    if user: # The method returns `True` when the user was banned and `False` when wasn't.
        print("User was successfully banned!")
    else:
        print("Hmmm.... There aren't any errors, but user wasn't banned...")
```

### Unbans user
```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

# Note that you MUST be a developer of the API to use this method!

from pynepcord.base import SharpSession

if __name__ == '__main__':
    key = 'your-api-token'
    session = SharpSession(key)
    user = session.unban(560529834325966858)  # Unbans the user with ID 560529834325966858.
    if user: # The method returns `True` when the user was unbanned and `False` when wasn't.
        print("User was successfully unbanned!")
    else:
        print("Hmmm.... There aren't any errors, but user wasn't unbanned...")
```

### async/await variant
```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

import asyncio
from pynepcord.aio import SharpSession
from pynepcord.errors import UserNotFound

async def main():
    key = 'your-api-token'
    session = SharpSession(key)
    try:
        user = await session.check(560529834325966858) # Checks if the user with ID 560529834325966858 in Sharp
    except UserNotFound: # If the user wasn't found, it returns an error
        print("The user wasn't found :klass:")
    else:
        print(user)

if __name__ == "__main__":
    asyncio.run(main())
```